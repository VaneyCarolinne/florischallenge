# florisChallenge

## ¿Cómo solucione el problema?
Utilice la arquitectura de software DDD (Domain Driven Desing), para abordar el problema y 
hacer el código de una manera que fuese fácil de leer. Elegí el lenguaje de programación Go
por sus habilidades de rendimiento y porque también desde que lo aprendí me encantó resolver problemas con él.

Entrando ya en la aplicación lo que hice fue crear 4 paquetes dentro del paquete principal main
para utilizar la arquitectura en 4 capas:

-Domain: Aquí tendremos todas nuestras entidades con lo que modelamos cada uno de los objetos del programa.

-Application: Este paquete contiene todo lo que sería la lógica necesaria de cada una de las acciones que tiene
nuestro proyecto, como por ejemplo leer un archivo, especificar que hace cada comando, hacer búsquedas, etc.

-Presentation: En este paquete colocamos nuestros controladores, dichos controladores se encargaran de llamar 
las funciones del paquete de aplicación para su uso según su caso.

-Infrastructure: En este paquete vendría lo que sería toda la configuración de alguna base de datos,
sin embargo, como no usamos bases de datos para resolver el problema lo dejamos vacío.

Por último y no menos importante está el paquete main el cual hace la llamada a los controladores
para que muestren la solución del problema ya sea por pantalla o en un archivo que se llamara report.txt

**_IMPORTANTE_**: Como entrada estándar debemos correr el programa y colocar
la ruta específica del archivo como por ejemplo: /Users/vanessacruz/go/src/forisChallenge/prueba.txt 

## ¿Cuáles fueron mis decisiones para abordar el problema?
Utilice los tipos de datos **struct** para modelar lo que sería cada uno de los casos de uso del programa,
como por ejemplo:

**Mi dominio** está basado en listas de **structs**,
dichas listas podemos tener el universo de los estudiantes registrados,
también tendríamos el universo del conjunto de estudiantes que asistieron a clases en un horario en especifico,
así como también tenemos una lista de cada una de las filas y las columnas escritas en el archivo de entrada,
para descifrar, cuál comando está escrito por cada linea. 
De esta manera, recorremos sobre todas las listas y hacemos búsquedas y ordenamientos sobre ellas para generar 
el reporte final.

**La aplicación de la solución** está constituida por varios paquetes, denominados según la acción y el caso de uso
para resolver una tarea. Por ejemplo: el paquete fileReader se encarga de leer el archivo y procesar la información que
hay dentro de él en dos funciones siguiendo así el principio de única responsabilidad basado en SOLID.

**Unit Tests**
Tome como caso de prueba, un caso de uso válido según el fin que tiene cada función en cada uno de sus paquetes
por lo cual cada una de ellas tiene una expectativa sencilla para así evitar que cualquier modificación pudiese romper
la función, ya que la prueba _lo detuviese_.

**Presentation** se encarga de separar el problema en dos controladores
uno para leer y registrar la data que recibió del archivo, y el otro para procesar los datos
y crear el reporte.


## ¿Cómo pruebo el programa en go?
Primero deberás descargar el programa en tu carpeta
src de go, es decir: donde tengas instalado Go, buscas esa carpeta
y dentro de ella debe haber una carpeta llamada src, allí corres los siguientes comandos:

******
Compila el programa en tu Sistema Operativo
******

```
cd forisChallenge/main
go build -o program-executor/foris
```

******
Si tienes MAC o LINUX
******
```
cd forisChallenge/main/program-executor
./foris
```

******
Si tienes Windows
******

Solo debes correr el .exe dando doble click

******
**Corriendo el programa**
******
Debera salirte algo en pantalla así:
```
Por favor ingresa la ruta del archivo
```
Allí colocas la ruta del archivo como en el ejemplo
y listo ves como funciona!

**Puedes ver un ejemplo de como funciona en la siguiente url:**
https://docs.google.com/document/d/1--kWxxba0m43e7fPVAeIia1rbznJFOS4Jg9ZkX_i3AE/edit?usp=sharing

## NOTA
En caso de tener MAC puedes intentar probando con
el compilado **foris** que está en la carpeta **program-executor**

**Cómo correr las pruebas Unitarias:**

_Correrlas todas al mismo tiempo:_
```
go test -cover ./... 
```
Correr una por una (Debes posicionarte en la ruta donde se encuentre la prueba)
```
cd forisChallenge/main/application/sort
go test -run TestSortReport
```
