package main

import (
	"fmt"
	"forisChallenge/main/domain/entity"
	"forisChallenge/main/presentation/controller"
	"log"
	"os"
	"strconv"
)

func main() {
	var fileRoute string
	fmt.Println("Por favor ingresa la ruta del archivo")
	fmt.Scanf("%s", &fileRoute)
	var studentClassData entity.StudentClassDataEntity
	studentClassData = controller.CommandController(fileRoute)
	var report []entity.ReportEntity
	report = controller.ReportController(studentClassData)
	fmt.Println("\nReporte:")
	PrintReport(report)
	SaveReport(report)
}

func SaveReport(report []entity.ReportEntity) {
	reportString := ""
	for i := 0; i < len(report); i++ {
		reportString += report[i].Student.Name + ": "
		reportString += strconv.Itoa(int(report[i].AssistanceInMinutes)) + " minutes "
		if report[i].AssistanceInDays > 0 {
			if report[i].AssistanceInDays > 1 {
				reportString += "in " + strconv.Itoa(report[i].AssistanceInDays) + " days\n"
			} else {
				reportString += "in " + strconv.Itoa(report[i].AssistanceInDays) + " day\n"
			}
		} else {
			reportString += "\n"
		}
	}

	f, err := os.Create("report.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	_, err2 := f.WriteString(reportString)
	if err2 != nil {
		log.Fatal(err2)
	}
}

func PrintReport(report []entity.ReportEntity) {
	for i := 0; i < len(report); i++ {
		fmt.Print(report[i].Student.Name + ": ")
		fmt.Print(strconv.Itoa(int(report[i].AssistanceInMinutes)) + " minutes ")
		if report[i].AssistanceInDays > 0 {
			if report[i].AssistanceInDays > 1 {
				fmt.Println("in " + strconv.Itoa(report[i].AssistanceInDays) + " days")
			} else {
				fmt.Println("in " + strconv.Itoa(report[i].AssistanceInDays) + " day")
			}
		} else {
			fmt.Println("")
		}
	}
}
