package command

import (
	"forisChallenge/main/domain/entity"
	"testing"
)

var commandRowStudent = entity.FileReaderEntity{
	EndHour:     "22:00",
	InitHour:    "19:30",
	WeekNumber:  "1",
	ClassRoom:   "C300",
	StudentName: "Vanessa",
	Command:     "Student",
}

var students []entity.StudentEntity

func TestStudentExecute(t *testing.T) {
	expected := true
	StudentExecute(commandRowStudent, &students)
	got := len(students) > 0
	if got != expected {
		t.Errorf("Expected: %v, got: %v", expected, got)
	}
}
