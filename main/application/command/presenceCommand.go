package command

import (
	"forisChallenge/main/domain/entity"
	"strconv"
	"strings"
)

const (
	Lunes     int64 = 1
	Martes          = 2
	Miercoles       = 3
	Jueves          = 4
	Viernes         = 5
	Sabado          = 6
	Domingo         = 7
)

func PresenceExecute(commandRow entity.FileReaderEntity, studentList []entity.StudentEntity, classroomAssistanceList *[]entity.ClassroomAssistanceEntity) {
	for i := 0; i < len(studentList); i++ {
		if commandRow.StudentName == studentList[i].Name {
			weekNumber, _ := strconv.ParseInt(commandRow.WeekNumber, 10, 0)
			studentAssistance := entity.ClassroomAssistanceEntity{
				Student: entity.StudentEntity{
					Name: commandRow.StudentName,
				},
				MinutesAssistance: GetMinutesAssistance(commandRow.InitHour, commandRow.EndHour),
				Classroom:         commandRow.ClassRoom,
				DayName:           GetDayByNumberWeek(weekNumber),
			}
			*classroomAssistanceList = append(*classroomAssistanceList, studentAssistance)
		}
	}
}

func GetMinutesAssistance(initHour string, endHour string) int64 {
	initHH, _ := strconv.ParseInt(strings.Split(initHour, ":")[0], 10, 0)
	initMM, _ := strconv.ParseInt(strings.Split(initHour, ":")[1], 10, 0)
	endHH, _ := strconv.ParseInt(strings.Split(endHour, ":")[0], 10, 0)
	endMM, _ := strconv.ParseInt(strings.Split(endHour, ":")[1], 10, 0)
	diffHH := endHH - initHH
	diffMM := endMM - initMM
	minutes := (diffHH * 60) + diffMM
	return minutes
}

func GetDayByNumberWeek(weekNumber int64) string {
	switch weekNumber {
	case Lunes:
		return "Lunes"
	case Martes:
		return "Martes"
	case Miercoles:
		return "Miercoles"
	case Jueves:
		return "Jueves"
	case Viernes:
		return "Viernes"
	case Sabado:
		return "Sabado"
	case Domingo:
		return "Domingo"
	}
	return ""
}
