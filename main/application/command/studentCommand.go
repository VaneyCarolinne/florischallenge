package command

import (
	"forisChallenge/main/domain/entity"
)

func StudentExecute(commandRow entity.FileReaderEntity, studentList *[]entity.StudentEntity) {
	student := entity.StudentEntity{
		Name: commandRow.StudentName,
	}
	*studentList = append(*studentList, student)
}
