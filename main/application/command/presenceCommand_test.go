package command

import (
	"forisChallenge/main/domain/entity"
	"testing"
)

var student = entity.StudentEntity{
	Name: "Vanessa",
}

var commandRow = entity.FileReaderEntity{
	EndHour:     "22:00",
	InitHour:    "19:30",
	WeekNumber:  "1",
	ClassRoom:   "C300",
	StudentName: "Vanessa",
	Command:     "Presence",
}

var studentList []entity.StudentEntity
var classroomAssistance []entity.ClassroomAssistanceEntity

func TestGetMinutesAssistance(t *testing.T) {
	expected := true
	minutes := GetMinutesAssistance("19:30", "22:00")
	got := minutes > 0
	if got != expected {
		t.Errorf("Expected: %v, got: %v", expected, got)
	}
}

func TestGetDayByNumberWeek(t *testing.T) {
	expected := true
	weekName := GetDayByNumberWeek(2)
	got := weekName == "Martes"
	if got != expected {
		t.Errorf("Expected: %v, got: %v", expected, got)
	}
}

func TestPresenceExecute(t *testing.T) {
	expected := true
	InitData()
	PresenceExecute(commandRow, studentList, &classroomAssistance)
	got := len(classroomAssistance) > 0
	if got != expected {
		t.Errorf("Expected: %v, got: %v", expected, got)
	}
}

func InitData() {
	studentList = append(studentList, student)
}
