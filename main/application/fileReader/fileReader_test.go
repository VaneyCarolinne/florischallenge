package fileReader

import (
	"bytes"
	"os/exec"
	"strings"
	"testing"
)

func TestFileReading(t *testing.T) {
	expected := true
	_, route, _ := commandExec()
	route = strings.Split(route, "\n")[0]
	list := FileReading(route + "/prueba.txt")
	got := len(list) > 0
	if got != expected {
		t.Errorf("Expected: %v, got: %v", expected, got)
	}
}

func TestGetFileReaderFromFileRow(t *testing.T) {
	expected := true
	fileReaderStudent := GetFileReaderFromFileRow("Student Vanessa")
	fileReaderPresence := GetFileReaderFromFileRow("Presence Marco 1 09:02 10:17 R100")
	got := fileReaderStudent.Command == "Student" && fileReaderPresence.Command == "Presence"
	if got != expected {
		t.Errorf("Expected: %v, got: %v", expected, got)
	}
}

func commandExec() (error, string, string) {
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd := exec.Command("bash", "-c", "pwd")
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	return err, stdout.String(), stderr.String()
}
