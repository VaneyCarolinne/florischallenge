package fileReader

import (
	"bufio"
	"fmt"
	"forisChallenge/main/domain/entity"
	"os"
	"strings"
)

func FileReading(fileRoute string) []entity.FileReaderEntity {
	file, err := os.Open(fileRoute)
	var list []entity.FileReaderEntity
	if err != nil {
		fmt.Println("File reading error", err)
		return list
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		list = append(list, GetFileReaderFromFileRow(scanner.Text()))
	}
	if err := scanner.Err(); err != nil {
		fmt.Println("File reading error", err)
	}
	return list
}

func GetFileReaderFromFileRow(row string) entity.FileReaderEntity {
	columns := strings.Split(row, " ")
	var fileReader entity.FileReaderEntity
	if len(columns) > 2 {
		fileReader.Command = columns[0]
		fileReader.StudentName = columns[1]
		fileReader.WeekNumber = columns[2]
		fileReader.InitHour = columns[3]
		fileReader.EndHour = columns[4]
		fileReader.ClassRoom = columns[5]
	} else {
		fileReader.Command = columns[0]
		fileReader.StudentName = columns[1]
	}
	return fileReader
}
