package sort

import "forisChallenge/main/domain/entity"

func SortReport(report *[]entity.ReportEntity) {
	finalReport := *report
	aux := entity.ReportEntity{}
	for i := 0; i < len(finalReport); i++ {
		for j := 0; j < len(finalReport); j++ {
			if finalReport[i].AssistanceInMinutes > finalReport[j].AssistanceInMinutes {
				aux = finalReport[i]
				finalReport[i] = finalReport[j]
				finalReport[j] = aux
			}
		}
	}
	*report = finalReport
}
