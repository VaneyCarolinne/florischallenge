package sort

import (
	"forisChallenge/main/domain/entity"
	"testing"
)

var studentHigher = entity.StudentEntity{
	Name: "Marco",
}

var studentSmaller = entity.StudentEntity{
	Name: "Fran",
}

var studentReportHigher = entity.ReportEntity{
	Student:             studentHigher,
	AssistanceInDays:    1,
	AssistanceInMinutes: 20,
}

var studentReportSmaller = entity.ReportEntity{
	Student:             studentSmaller,
	AssistanceInDays:    1,
	AssistanceInMinutes: 10,
}

var reportTest []entity.ReportEntity

func TestSortReport(t *testing.T) {
	InitData()
	expected := true
	SortReport(&reportTest)
	got := reportTest[0].AssistanceInMinutes > reportTest[1].AssistanceInMinutes
	if got != expected {
		t.Errorf("Expected: %v, got: %v", expected, got)
	}
}

func InitData() {
	reportTest = append(reportTest, studentReportSmaller)
	reportTest = append(reportTest, studentReportHigher)
}
