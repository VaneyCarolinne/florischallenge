package reportAssistance

import (
	"fmt"
	"forisChallenge/main/domain/entity"
	"testing"
)

var student = entity.StudentEntity{
	Name: "Marco",
}

var absentStudent = entity.StudentEntity{
	Name: "Fran",
}

var studentAssistance = entity.ClassroomAssistanceEntity{
	Student:           student,
	MinutesAssistance: 20,
	DayName:           "Miercoles",
	Classroom:         "F100",
}

var studentReport = entity.ReportEntity{
	Student:             student,
	AssistanceInDays:    1,
	AssistanceInMinutes: 40,
}

var classroomAssistance []entity.ClassroomAssistanceEntity
var reportAbsencesTest []entity.ReportEntity
var reportAssistanceTest []entity.ReportEntity
var reportTest []entity.ReportEntity
var studentList []entity.StudentEntity

func TestCreateReportAssistance(t *testing.T) {
	InitData()
	expected := true
	reportTest = CreateReportAssistance(studentList, classroomAssistance)
	got := len(reportTest) > 0
	if got != expected {
		t.Errorf("Expected: %v, got: %v", expected, got)
	}
}

func TestAddAbsences(t *testing.T) {
	InitData()
	expected := true
	AddAbsences(studentList, classroomAssistance, &reportAbsencesTest)
	fmt.Println("absences", reportAbsencesTest)
	got := reportAbsencesTest[0].AssistanceInMinutes == 0
	if got != expected {
		t.Errorf("Expected: %v, got: %v", expected, got)
	}
}

func TestAddAssistance(t *testing.T) {
	InitData()
	expected := true
	reportAssistanceTest = append(reportAssistanceTest, studentReport)
	AddAssistance(student, studentAssistance, &reportAssistanceTest)
	got := reportAssistanceTest[0].AssistanceInMinutes > studentReport.AssistanceInMinutes
	if got != expected {
		t.Errorf("Expected: %v, got: %v", expected, got)
	}
}

func TestAddInitialAssistance(t *testing.T) {
	InitData()
	expected := true
	AddInitialAssistance(student, studentAssistance, &reportTest)
	got := len(reportTest) > 0
	if got != expected {
		t.Errorf("Expected: %v, got: %v", expected, got)
	}
}

func TestAddToTheReport(t *testing.T) {
	InitData()
	expected := true
	AddToTheReport(student, studentAssistance, &reportTest)
	got := len(reportTest) > 0
	if got != expected {
		t.Errorf("Expected: %v, got: %v", expected, got)
	}
}

func InitData() {
	classroomAssistance = append(classroomAssistance, studentAssistance)
	studentList = append(studentList, absentStudent)
	studentList = append(studentList, student)
}
