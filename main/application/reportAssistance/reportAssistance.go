package reportAssistance

import (
	"forisChallenge/main/application/search"
	"forisChallenge/main/application/sort"
	"forisChallenge/main/domain/entity"
)

func CreateReportAssistance(studentList []entity.StudentEntity, classroomAssistance []entity.ClassroomAssistanceEntity) []entity.ReportEntity {
	var report []entity.ReportEntity
	AddAbsences(studentList, classroomAssistance, &report)

	for j := 0; j < len(studentList); j++ {
		for k := 0; k < len(classroomAssistance); k++ {
			if studentList[j].Name == classroomAssistance[k].Student.Name {
				AddToTheReport(studentList[j], classroomAssistance[k], &report)
			}
		}
	}
	sort.SortReport(&report)

	return report
}

func AddAbsences(studentList []entity.StudentEntity, classroomAssistance []entity.ClassroomAssistanceEntity, report *[]entity.ReportEntity) {
	for l := 0; l < len(studentList); l++ {
		if !search.FindStudentAssistance(studentList[l].Name, classroomAssistance) && !search.FindStudentInReport(studentList[l].Name, *report) {
			*report = append(*report, entity.ReportEntity{
				Student:             studentList[l],
				AssistanceInDays:    0,
				AssistanceInMinutes: 0,
			})
		}
	}
}

func AddInitialAssistance(student entity.StudentEntity, classroomAssistance entity.ClassroomAssistanceEntity, report *[]entity.ReportEntity) {
	*report = append(*report, entity.ReportEntity{
		Student:             student,
		AssistanceInDays:    1,
		AssistanceInMinutes: classroomAssistance.MinutesAssistance,
	})
}

func AddAssistance(student entity.StudentEntity, classroomAssistance entity.ClassroomAssistanceEntity, report *[]entity.ReportEntity) {
	reportSearch := *report
	for i := 0; i < len(reportSearch); i++ {
		if student.Name == reportSearch[i].Student.Name {
			reportSearch[i].AssistanceInDays = reportSearch[i].AssistanceInDays + 1
			reportSearch[i].AssistanceInMinutes = reportSearch[i].AssistanceInMinutes + classroomAssistance.MinutesAssistance
		}
	}
	*report = reportSearch
}

func AddToTheReport(student entity.StudentEntity, classroomAssistance entity.ClassroomAssistanceEntity, report *[]entity.ReportEntity) {
	finalReport := *report
	if len(finalReport) == 0 || !search.FindStudentInReport(student.Name, finalReport) {
		AddInitialAssistance(student, classroomAssistance, &finalReport)
	} else {
		AddAssistance(student, classroomAssistance, &finalReport)
	}
	*report = finalReport
}
