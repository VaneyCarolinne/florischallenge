package search

import (
	"forisChallenge/main/domain/entity"
	"testing"
)

var student = entity.StudentEntity{
	Name: "Marco",
}

var studentAssistance = entity.ClassroomAssistanceEntity{
	Student:           student,
	MinutesAssistance: 20,
	DayName:           "Miercoles",
	Classroom:         "F100",
}

var studentReport = entity.ReportEntity{
	Student:             student,
	AssistanceInDays:    1,
	AssistanceInMinutes: 20,
}

var classroomAssistance []entity.ClassroomAssistanceEntity
var reportTest []entity.ReportEntity

func TestFindStudentAssistance(t *testing.T) {
	InitData()
	expected := true
	got := FindStudentAssistance("Marco", classroomAssistance)
	if got != expected {
		t.Errorf("Expected: %v, got: %v", expected, got)
	}
}

func TestFindStudentInReport(t *testing.T) {
	InitData()
	expected := true
	got := FindStudentInReport("Marco", reportTest)
	if got != expected {
		t.Errorf("Expected: %v, got: %v", expected, got)
	}
}

func InitData() {
	classroomAssistance = append(classroomAssistance, studentAssistance)
	reportTest = append(reportTest, studentReport)
}
