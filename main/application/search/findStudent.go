package search

import "forisChallenge/main/domain/entity"

func FindStudentInReport(name string, report []entity.ReportEntity) bool {
	isExist := false
	for i := 0; i < len(report); i++ {
		if name == report[i].Student.Name {
			isExist = true
		}
	}
	return isExist
}

func FindStudentAssistance(name string, assistance []entity.ClassroomAssistanceEntity) bool {
	isPresent := false
	for i := 0; i < len(assistance); i++ {
		if name == assistance[i].Student.Name {
			isPresent = true
		}
	}
	return isPresent
}
