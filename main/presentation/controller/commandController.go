package controller

import (
	"forisChallenge/main/application/command"
	"forisChallenge/main/application/fileReader"
	"forisChallenge/main/domain/entity"
)

const StudentCommand = "Student"
const PresenceCommand = "Presence"

func CommandController(fileRoute string) entity.StudentClassDataEntity {
	list := fileReader.FileReading(fileRoute)
	var studentList []entity.StudentEntity
	var classroomAssistance []entity.ClassroomAssistanceEntity
	for i := 0; i < len(list); i++ {
		if list[i].Command == StudentCommand {
			command.StudentExecute(list[i], &studentList)
		}
		if list[i].Command == PresenceCommand {
			command.PresenceExecute(list[i], studentList, &classroomAssistance)
		}
	}

	return entity.StudentClassDataEntity{
		StudentAssistanceList: classroomAssistance,
		StudentList:           studentList,
	}

}
