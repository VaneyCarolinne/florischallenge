package controller

import (
	"forisChallenge/main/application/reportAssistance"
	"forisChallenge/main/domain/entity"
)

func ReportController(studentClassData entity.StudentClassDataEntity) []entity.ReportEntity {
	return reportAssistance.CreateReportAssistance(studentClassData.StudentList, studentClassData.StudentAssistanceList)
}
