package entity

type StudentClassDataEntity struct {
	StudentList           []StudentEntity
	StudentAssistanceList []ClassroomAssistanceEntity
}
