package entity

type ClassroomAssistanceEntity struct {
	Student           StudentEntity
	DayName           string
	Classroom         string
	MinutesAssistance int64
}
