package entity

type FileReaderEntity struct {
	Command     string
	StudentName string
	WeekNumber  string
	InitHour    string
	EndHour     string
	ClassRoom   string
}
