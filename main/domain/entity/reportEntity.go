package entity

type ReportEntity struct {
	Student             StudentEntity
	AssistanceInMinutes int64
	AssistanceInDays    int
}
